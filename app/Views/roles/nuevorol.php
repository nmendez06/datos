<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Nuevo rol</title>
	<link rel="stylesheet" href="<?php echo base_url('bootstrap/css/bootstrap.css')?>" type="text/css">
	<script type="text/javascript" src="<?php echo base_url('bootstrap/js/bootstrap.js')?>"></script>
</head>
<body>
	<a href="<?php echo base_url().'/roles'; ?>" class="btn btn-light">Regresar a Roles</a>
	<div class="container border">
		<h1>Nuevo rol</h1>
		<div class="form-group">
			<?php
				$atributos = ['class' => 'formRol', 'id' => 'formRol'];
				echo form_open(base_url().'/roles/insertar', $atributos);
			?>
			<div class="form-row">
				<div class="col">
					<?php
						$nombre = ['name' => 'nombreRol', 'id' => 'nombreRol', 'required'=>'required', 'class'=>'form-control'];
					  echo form_label('Nombre del rol: ', 'nombreRol');
						echo form_input($nombre);
					?>
				</div>
				<div class="col">
					<?php
						$desc = ['name' => 'descripcionRol', 'id' => 'descripcionRol', 'required'=>'required', 'class'=>'form-control'];
						echo form_label('Descripcion del rol: ', 'descripcionRol');
						echo form_input($desc);
					?>
				</div>
			</div>
			<div class="form-row">
				<div class="col">
					<?php
						$permisos = ['name' => 'permisos', 'id' => 'permisos', 'required'=>'required', 'class'=>'form-control'];
						echo form_label('Permisos: ', 'permisos');
						echo form_input($permisos);
					?>
				</div>
				<div class="col">
					<?php
						$empresa = ['name' => 'Empresas_idEmpresa', 'id' => 'Empresas_idEmpresa', 'required'=>'required', 'class'=>'form-control'];
						foreach ($empresas as $em) {
							$options [$em['idEmpresa']]  = $em['nombreEmpresa'];
						}
						echo form_label('Empresa de donde proviene el rol: ', 'Empresas_idEmpresa');
						echo form_dropdown($empresa, $options);
					?>
				</div>
			</div>
						<?php
							echo form_submit('enviar', 'Agregar rol');
							echo form_close();
						?>

		</div>
	</div>
</body>
</html>
