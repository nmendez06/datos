<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Ver rol</title>
	<link rel="stylesheet" href="<?php echo base_url('bootstrap/css/bootstrap.css')?>" type="text/css">
	<script type="text/javascript" src="<?php echo base_url('bootstrap/js/bootstrap.js')?>"></script>
</head>
<body>
	<a href="<?php echo base_url().'/roles'; ?>" class="btn btn-light">Regresar a Roles</a>
	<div class="container border">
		<h1>Información del rol</h1>
		<div class="form-group">
			<?php
				echo form_open();
			?>
			<div class="form-row">
				<div class="col">
					<?php
						$nombre = ['name' => 'nombreRol', 'id' => 'nombreRol', 'readonly'=>'readonly', 'class'=>'form-control', 'value' => $rol['nombreRol']];
					  echo form_label('Nombre del rol: ', 'nombreRol');
						echo form_input($nombre);
					?>
				</div>
				<div class="col">
					<?php
						$desc = ['name' => 'descripcionRol', 'id' => 'descripcionRol', 'readonly'=>'readonly', 'class'=>'form-control', 'value'=>$rol['descripcionRol']];
						echo form_label('Descripcion del rol: ', 'descripcionRol');
						echo form_input($desc);
					?>
				</div>
			</div>
			<div class="form-row">
				<div class="col">
					<?php
						$permisos = ['name' => 'permisos', 'id' => 'permisos', 'readonly'=>'readonly', 'class'=>'form-control', 'value'=>$rol['permisos']];
						echo form_label('Permisos: ', 'permisos');
						echo form_input($permisos);
					?>
				</div>
				<div class="col">
					<?php
						$empresa = ['name' => 'Empresas_idEmpresa', 'id' => 'Empresas_idEmpresa', 'readonly'=>'readonly', 'class'=>'form-control'];
						foreach ($empresas as $em) {
							$options [$em['idEmpresa']]  = $em['nombreEmpresa'];
						}
						echo form_label('Empresa de donde proviene el rol: ', 'Empresas_idEmpresa');
						echo form_dropdown($empresa, $options, $rol['Empresas_idEmpresa']);
					?>
				</div>
			</div>
						<?php
							echo form_close();
						?>

		</div>
	</div>
</body>
</html>
