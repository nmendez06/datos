<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Nuevo empleado</title>
	<link rel="stylesheet" href="<?php echo base_url('bootstrap/css/bootstrap.css')?>" type="text/css">
	<script type="text/javascript" src="<?php echo base_url('bootstrap/js/bootstrap.js')?>"></script>
	<script type="text/javascript" src="<?php echo base_url('jquery/jquery-3.5.1.min.js')?>"></script>
</head>
<body>
	<a href="<?php echo base_url().'/empleados'; ?>" class="btn btn-light">Regresar a Empleados</a>
	<div class="container border">
		<h1>Nuevo empleado</h1>
		<div class="form-group">
			<?php
				$atributos = ['class' => 'formEmpleado', 'id' => 'formEmpleado'];
				echo form_open(base_url().'/empleados/insertar', $atributos);
			?>
			<div class="form-row">
				<div class="col">
					<?php
						$nombre = ['name' => 'nombres', 'id' => 'nombres', 'required'=>'required', 'class'=>'form-control'];
					  echo form_label('Nombre del empleado: ', 'nombres');
						echo form_input($nombre);
					?>
				</div>
				<div class="col">
					<?php
						$apellidos = ['name' => 'apellidos', 'id' => 'apellidos', 'required'=>'required', 'class'=>'form-control'];
						echo form_label('Apellidos del empleado: ', 'apellidos');
						echo form_input($apellidos);
					?>
				</div>
			</div>
			<div class="form-row">
				<div class="col">
					<?php
						$dui = ['name' => 'dui', 'id' => 'dui', 'required'=>'required', 'class'=>'form-control'];
						echo form_label('DUI: ', 'dui');
						echo form_input($dui);
					?>
				</div>
				<div class="col">
					<?php
						$nit = ['name' => 'nit', 'id' => 'nit', 'required'=>'required', 'class'=>'form-control'];
						echo form_label('NIT: ', 'nit');
						echo form_input($nit);
					?>
				</div>
				<div class="col">
					<?php
						$estado = ['name' => 'estado', 'id' => 'estado', 'required'=>'required', 'class'=>'form-control'];
						$options = ['Activo' => 'Activo', 'Inactivo' => 'Inactivo'];
						echo form_label('Estado: ', 'estado');
						echo form_dropdown($estado, $options);
					?>
				</div>
			</div>
			<div class="form-row">
				<div class="col">
					<?php
						$empresa = ['name' => 'Empresas_idEmpresa', 'id' => 'Empresas_idEmpresa', 'required'=>'required', 'class'=>'form-control'];
						foreach ($empresas as $em) {
							$options1 [$em['idEmpresa']]  = $em['nombreEmpresa'];
						}
						echo form_label('Empresa de donde proviene el empleado: ', 'Empresas_idEmpresa');
						echo form_dropdown($empresa, $options1);
					?>
				</div>
				<div class="col">
					<?php
						$rolemp = ['name' => 'Roles_idRol', 'id' => 'Roles_idRol', 'required'=>'required', 'class'=>'form-control'];
						foreach ($roles as $rol) {
							$options2 [$rol['idRol']]  = $rol['nombreRol'];
						}
						echo form_label('Rol del empleado: ', 'Roles_idRol');
						echo form_dropdown($rolemp, $options2);
					?>
				</div>
			</div>
						<?php
							echo form_submit('enviar', 'Agregar empleado');
							echo form_close();
						?>

		</div>
	</div>
</body>
</html>
<!--script>
	$(document).ready(function(){
			$('#Empresas_idEmpresa').change(function(){
				var empid = $('#Empresas_idEmpresa').val();

				$.ajax({
					url: "<?php //echo base_url().'/empleados/rolesEmpresa'?>",
					method: "POST",
					data:{id:empid},
					success:function(data){
						console.log(data);
						alert(empid);
						$('#Roles_idRol').html(data);
					}
				})
			});
	});
</script-->
