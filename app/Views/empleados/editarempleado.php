<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Editar empresa</title>
	<link rel="stylesheet" href="<?php echo base_url('bootstrap/css/bootstrap.css')?>" type="text/css">
	<script type="text/javascript" src="<?php echo base_url('bootstrap/js/bootstrap.js')?>"></script>
</head>
<body>
	<a href="<?php echo base_url().'/empleados'; ?>" class="btn btn-light">Regresar a Empleados</a>
	<div class="container border">
		<h1>Editar empleado</h1>
		<div class="form-group">
			<?php
				$atributos = ['class' => 'formEmpleado', 'id' => 'formEmpleado'];
				echo form_open(base_url().'/empleados/actualizar', $atributos);
				echo form_hidden('idEmpleado', $empleado['idEmpleado']);
			?>
			<div class="form-row">
				<div class="col">
					<?php
						$nombre = ['name' => 'nombres', 'id' => 'nombres', 'required'=>'required', 'class'=>'form-control', 'value'=>$empleado['nombres']];
						echo form_label('Nombre del empleado: ', 'nombres');
						echo form_input($nombre);
					?>
				</div>
				<div class="col">
					<?php
						$apellidos = ['name' => 'apellidos', 'id' => 'apellidos', 'required'=>'required', 'class'=>'form-control', 'value'=>$empleado['apellidos']];
						echo form_label('Apellidos del empleado: ', 'apellidos');
						echo form_input($apellidos);
					?>
				</div>
			</div>
			<div class="form-row">
				<div class="col">
					<?php
						$dui = ['name' => 'dui', 'id' => 'dui', 'required'=>'required', 'class'=>'form-control', 'value'=>$empleado['dui']];
						echo form_label('DUI: ', 'dui');
						echo form_input($dui);
					?>
				</div>
				<div class="col">
					<?php
						$nit = ['name' => 'nit', 'id' => 'nit', 'required'=>'required', 'class'=>'form-control', 'value'=>$empleado['nit']];
						echo form_label('NIT: ', 'nit');
						echo form_input($nit);
					?>
				</div>
				<div class="col">
					<?php
						$estado = ['name' => 'estado', 'id' => 'estado', 'required'=>'required', 'class'=>'form-control'];
						$options = ['Activo' => 'Activo', 'Inactivo' => 'Inactivo'];
						echo form_label('Estado: ', 'estado');
						echo form_dropdown($estado, $options, $empleado["estado"]);
					?>
				</div>
			</div>
			<div class="form-row">
				<div class="col">
					<?php
						$empresa = ['name' => 'Empresas_idEmpresa', 'id' => 'Empresas_idEmpresa', 'required'=>'required', 'class'=>'form-control'];
						foreach ($empresas as $em) {
							$options [$em['idEmpresa']]  = $em['nombreEmpresa'];
						}
						echo form_label('Empresa a la que pertenece: ', 'Empresas_idEmpresa');
						echo form_dropdown($empresa, $options, $empleado['Empresas_idEmpresa']);
					?>
				</div>
				<div class="col">
					<?php
						$rolemp = ['name' => 'Roles_idRol', 'id' => 'Roles_idRol', 'required'=>'required', 'class'=>'form-control'];
						foreach ($roles as $rol) {
							$options2 [$rol['idRol']]  = $rol['nombreRol'];
						}
						echo form_label('Rol dentro de la empresa: ', 'Roles_idRol');
						echo form_dropdown($rolemp, $options2, $empleado['Roles_idRol']);
					?>
				</div>
			</div>
						<?php
						echo form_submit('enviar', 'Guardar Cambios');
							echo form_close();
						?>

		</div>
	</div>
</body>
</html>
