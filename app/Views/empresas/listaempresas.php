<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Listado de empresas</title>
	<link rel="stylesheet" href="<?php echo base_url('bootstrap/css/bootstrap.css')?>" type="text/css">
	<script type="text/javascript" src="<?php echo base_url('bootstrap/js/bootstrap.js')?>"></script>
</head>
<body>
	<div class="wrapper">
    <nav>
        <ul class="list-group">
					<li><a href="<?php echo base_url().'/empresas'; ?>" class="btn btn-light">Empresas</a></li>
					<li><a href="<?php echo base_url().'/roles'; ?>" class="btn btn-light">Roles</a></li>
					<li><a href="<?php echo base_url().'/empleados'; ?>" class="btn btn-light">Empleados</a></li>
				</ul>
    </nav>
	</div>
	<div class="container border">
		<div class="d-flex w-100 p-3 justify-content-between" id="superior">
			<h1>Empresas</h1>
			<a href="<?php echo base_url().'/empresas/nuevo' ?>" class="btn btn-primary">Nueva Empresa</a>
		</div>
		<div id="cuerpo" class="d-flex justify-content-center">
			<table class="table table-hover table-bordered">
				<tr>
					<th>ID</th>
					<th>Nombre</th>
					<th>Acciones</th>
					<?php
						foreach ($empresas as $em) {
					?>
					<tr>
						<td><?php echo $em['idEmpresa']; ?></td>
						<td><?php echo $em['nombreEmpresa']; ?></td>
						<td>
							<a href="<?php echo base_url().'/empresas/ver/'.$em['idEmpresa']; ?>" class="btn btn-primary">Ver</a>
							<a href="<?php echo base_url().'/empresas/editar/'.$em['idEmpresa']; ?>" class="btn btn-success">Editar</a>
							<a href="<?php echo base_url().'/empresas/eliminar/'.$em['idEmpresa']; ?>" class="btn btn-danger">Eliminar</a>
						</td>
					</tr>
					<?php } ?>
				</tr>
			</table>
		</div>
	</div>
</body>
</html>
