<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Nueva empresa</title>
	<link rel="stylesheet" href="<?php echo base_url('bootstrap/css/bootstrap.css')?>" type="text/css">
	<script type="text/javascript" src="<?php echo base_url('bootstrap/js/bootstrap.js')?>"></script>
</head>
<body>
	<a href="<?php echo base_url().'/empresas'; ?>" class="btn btn-light">Regresar a Empresas</a>
	<div class="container border">
		<h1>Nueva empresa</h1>
		<div class="form-group">
			<?php
				$atributos = ['class' => 'formEmpresa', 'id' => 'formEmpresa'];
				echo form_open(base_url().'/empresas/insertar', $atributos);
			?>
			<div class="form-row">
				<div class="col">
					<?php
						$nombre = ['name' => 'nombreEmpresa', 'id' => 'nombreEmpresa', 'required'=>'required', 'class'=>'form-control'];
					  echo form_label('Nombre de la empresa: ', 'nombreEmpresa');
						echo form_input($nombre);
					?>
				</div>
				<div class="col">
					<?php
						$nit = ['name' => 'nit', 'id' => 'nit', 'required'=>'required', 'class'=>'form-control'];
						echo form_label('Nit: ', 'nit');
						echo form_input($nit);
					?>
				</div>
			</div>
			<div class="form-row">
				<div class="col">
					<?php
						$telefono = ['name' => 'telefono', 'id' => 'telefono', 'required'=>'required', 'class'=>'form-control'];
						echo form_label('Teléfono de la empresa: ', 'telefono');
						echo form_input($telefono);
					?>
				</div>
				<div class="col">
					<?php
						$direccion = ['name' => 'direccion', 'id' => 'direccion', 'required'=>'required', 'class'=>'form-control'];
						echo form_label('Dirección: ', 'direccion');
						echo form_input($direccion);
					?>
				</div>
			</div>
			<div class="form-row">
				<div class="col">
					<?php
						$municipio = ['name' => 'municipio', 'id' => 'municipio', 'required'=>'required', 'class'=>'form-control'];
						echo form_label('Municipio: ', 'municipio');
						echo form_input($municipio);
					?>
					</div>
					<div class="col">
						<?php
							$departamento = ['name' => 'departamento', 'id' => 'departamento', 'required'=>'required', 'class'=>'form-control'];
							echo form_label('Departamento: ', 'departamento');
							echo form_input($departamento);
						?>
					</div>
				</div>
						<?php
							echo form_submit('enviar', 'Agregar empresa');
							echo form_close();
						?>

		</div>
	</div>
</body>
</html>
