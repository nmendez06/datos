<?php namespace App\Models;

use CodeIgniter\Model;

class EmpleadosModel extends Model
{
    protected $table      = 'empleados';
    protected $primaryKey = 'idEmpleado';

    protected $returnType     = 'array';
    //protected $useSoftDeletes = true;

    protected $allowedFields = ['idEmpleado', 'nombres', 'apellidos', 'dui', 'nit', 'estado', 'Empresas_idEmpresa', 'Roles_idRol'];

    //protected $useTimestamps = false;
    //protected $createdField  = 'created_at';
    //protected $updatedField  = 'updated_at';
    //protected $deletedField  = 'deleted_at';

  //  protected $validationRules    = [];
  //  protected $validationMessages = [];
  //  protected $skipValidation     = false;
}
