<?php namespace App\Models;

use CodeIgniter\Model;

class EmpresasModel extends Model
{
    protected $table      = 'empresas';
    protected $primaryKey = 'idEmpresa';

    protected $returnType     = 'array';
    //protected $useSoftDeletes = true;

    protected $allowedFields = ['idEmpresa', 'nombreEmpresa', 'nit', 'telefono', 'direccion', 'municipio', 'departamento'];

    //protected $useTimestamps = false;
    //protected $createdField  = 'created_at';
    //protected $updatedField  = 'updated_at';
    //protected $deletedField  = 'deleted_at';

  //  protected $validationRules    = [];
  //  protected $validationMessages = [];
  //  protected $skipValidation     = false;
}
