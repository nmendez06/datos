<?php namespace App\Controllers;

use App\Models\EmpresasModel;

class Empresas extends BaseController
{

	public function __construct()
  {
    helper('form');
  }

	public function index()
	{
		$em = new EmpresasModel();
		$datos['empresas'] = $em->findAll();
		return view('empresas/listaempresas', $datos);
	}

	public function ver($id)
  {
  	$em = new EmpresasModel();
    $dato['empresa'] = $em->where('idEmpresa', $id)->first();
		return view('empresas/verempresa', $dato);
  }

	public function eliminar($id)
	{
		$em = new EmpresasModel();
		$em->delete($id);
		return redirect()->to( base_url('empresas') );
	}

	public function nuevo()
	{
			return view('empresas/nuevaempresa');
	}

	public function insertar()
	{
		$em = new EmpresasModel();
		$datos = [
            'nombreEmpresa' => $this->request->getPost('nombreEmpresa'),
            'nit'  => $this->request->getPost('nit'),
            'telefono'  => $this->request->getPost('telefono'),
            'direccion'  => $this->request->getPost('direccion'),
            'municipio'  => $this->request->getPost('municipio'),
						'departamento'  => $this->request->getPost('departamento')
            ];
    $em->insert($datos);
		return redirect()->to( base_url('empresas') );
	}

	public function editar($id)
  {
  	$em = new EmpresasModel();
    $dato['empresa'] = $em->where('idEmpresa', $id)->first();
		return view('empresas/editarempresa', $dato);
  }

	public function actualizar()
	{
		$em = new EmpresasModel();
		$id = $this->request->getPost('idEmpresa');
		$datos = [
						'nombreEmpresa' => $this->request->getPost('nombreEmpresa'),
						'nit'  => $this->request->getPost('nit'),
						'telefono'  => $this->request->getPost('telefono'),
						'direccion'  => $this->request->getPost('direccion'),
						'municipio'  => $this->request->getPost('municipio'),
						'departamento'  => $this->request->getPost('departamento')
						];
		$em->update($id, $datos);
		return redirect()->to( base_url('empresas') );
	}

}
