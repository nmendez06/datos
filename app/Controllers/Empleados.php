<?php namespace App\Controllers;

use App\Models\EmpleadosModel;
use App\Models\EmpresasModel;
use App\Models\RolesModel;

class Empleados extends BaseController
{

	public function __construct()
  {
    helper('form');
  }

	public function index()
	{
		$emp = new EmpleadosModel();
		$datos['empleados'] = $emp->findAll();
		return view('empleados/listaempleados', $datos);
	}

	public function ver($id)
  {
  	$emp = new EmpleadosModel();
		$em = new EmpresasModel();
		$rm = new RolesModel();
    $dato['empleado'] = $emp->where('idEmpleado', $id)->first();
		$dato['empresas'] = $em->findAll();
		$dato['roles'] = $rm->findAll();
		return view('empleados/verempleado', $dato);
  }

	public function eliminar($id)
	{
		$emp = new EmpleadosModel();
		$emp->delete($id);
		return redirect()->to( base_url('empleados') );
	}

	public function nuevo()
	{
		$em = new EmpresasModel();
		$rm = new RolesModel();
		$datos['empresas'] = $em->findAll();
		$datos['roles'] = $rm->findAll();
		return view('empleados/nuevoempleado', $datos);
	}

/*public function rolesEmpresa(){
	$id = $this->request->getPost('id');
	$rm = new RolesModel();
	$datos = $rm->where('Empresas_idEmpresa', $id)->findAll();
	foreach ($datos as $rol) {
		$options2 [$rol['idRol']]  = $rol['nombreRol'];
	}
	$hola='hola';
	return $hola;
}
*/
	public function insertar()
	{
		$emp = new EmpleadosModel();
		$datos = [
            'nombres' => $this->request->getPost('nombres'),
            'apellidos'  => $this->request->getPost('apellidos'),
            'dui'  => $this->request->getPost('dui'),
						'nit'  => $this->request->getPost('nit'),
						'estado'  => $this->request->getPost('estado'),
						'Empresas_idEmpresa'  => $this->request->getPost('Empresas_idEmpresa'),
            'Roles_idRol'  => $this->request->getPost('Roles_idRol')
            ];
    $emp->insert($datos);
		return redirect()->to( base_url('empleados') );
	}

	public function editar($id)
  {
		$emp = new EmpleadosModel();
		$em = new EmpresasModel();
		$rm = new RolesModel();
		$dato['empleado'] = $emp->where('idEmpleado', $id)->first();
		$dato['empresas'] = $em->findAll();
		$dato['roles'] = $rm->findAll();
		return view('empleados/editarempleado', $dato);
  }

	public function actualizar()
	{
		$id = $this->request->getPost('idEmpleado');
		$emp = new EmpleadosModel();
		$datos = [
            'nombres' => $this->request->getPost('nombres'),
            'apellidos'  => $this->request->getPost('apellidos'),
            'dui'  => $this->request->getPost('dui'),
						'nit'  => $this->request->getPost('nit'),
						'estado'  => $this->request->getPost('estado'),
						'Empresas_idEmpresa'  => $this->request->getPost('Empresas_idEmpresa'),
            'Roles_idRol'  => $this->request->getPost('Roles_idRol')
            ];
    $emp->update($id, $datos);
		return redirect()->to( base_url('empleados') );
	}

}
