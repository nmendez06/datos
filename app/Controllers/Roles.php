<?php namespace App\Controllers;

use App\Models\RolesModel;
use App\Models\EmpresasModel;

class Roles extends BaseController
{

	public function __construct()
  {
    helper('form');
  }

	public function index()
	{
		$rm = new RolesModel();
		$datos['roles'] = $rm->findAll();
		return view('roles/listaroles', $datos);
	}

	public function ver($id)
  {
  	$rm = new RolesModel();
		$em = new EmpresasModel();
    $dato['rol'] = $rm->where('idRol', $id)->first();
		$dato['empresas'] = $em->findAll();
		return view('roles/verrol', $dato);
  }

	public function eliminar($id)
	{
		$rm = new RolesModel();
		$rm->delete($id);
		return redirect()->to( base_url('roles') );
	}

	public function nuevo()
	{
		$em = new EmpresasModel();
		$datos['empresas'] = $em->findAll();
		return view('roles/nuevorol', $datos);
	}

	public function insertar()
	{
		$rm = new RolesModel();
		$datos = [
            'nombreRol' => $this->request->getPost('nombreRol'),
            'descripcionRol'  => $this->request->getPost('descripcionRol'),
            'permisos'  => $this->request->getPost('permisos'),
            'Empresas_idEmpresa'  => $this->request->getPost('Empresas_idEmpresa')
            ];
    $rm->insert($datos);
		return redirect()->to( base_url('roles') );
	}

	public function editar($id)
  {
  	$rm = new RolesModel();
    $dato['rol'] = $rm->where('idrol', $id)->first();
		$em = new EmpresasModel();
		$dato['empresas'] = $em->findAll();
		return view('roles/editarrol', $dato);
  }

	public function actualizar()
	{
		$rm = new RolesModel();
		$id = $this->request->getPost('idRol');
		$datos = [
            'nombreRol' => $this->request->getPost('nombreRol'),
            'descripcionRol'  => $this->request->getPost('descripcionRol'),
            'permisos'  => $this->request->getPost('permisos'),
            'Empresas_idEmpresa'  => $this->request->getPost('Empresas_idEmpresa')
            ];
    $rm->update($id, $datos);
		return redirect()->to( base_url('roles') );
	}

}
